package fr.mastersid.spaceinvaders;

import android.content.Context;
import android.media.MediaPlayer;

public class Music {
    MediaPlayer menuMusic;
    MediaPlayer gameMusic;
    public Music(Context context){
        menuMusic = MediaPlayer.create(context, R.raw.menumusic);
        gameMusic = MediaPlayer.create(context, R.raw.gamemusic);

    }

    public void playMenuMusic(){
        menuMusic.start();
    }
    public void stopMenuMusic(){
        menuMusic.stop();
    }
    public void playGameMusic(){
        gameMusic.start();
    }
    public void stopGameMusic(){
        gameMusic.stop();
    }
    public void pauseGameMusic(){
        gameMusic.pause();
    }

}
